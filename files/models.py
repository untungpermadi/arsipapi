from django.db import models
# from .models import File

class File(models.Model):
    fileName = models.CharField(max_length=100, blank=True, default='')
    file = models.FileField(blank = False, null = False)
    uploadDate = models.DateTimeField(auto_now_add = True)
    idUser = models.IntegerField(blank = False, null = False)

    class Meta:
        ordering = ['uploadDate']

    def __str__(self):
        return self